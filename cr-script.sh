#!/bin/bash

#started_time=`date +%s`
#while [ $(( $(date +%s) - (5*60*60+0*15*60) )) -lt $started_time ]; do
#echo -e "$(date) and waiting."
#sleep 300
#done

calldir=$(dirname "$0")
#cd $calldir

start_time=`TZ='America/Los_Angeles' date +'%s' --date='thursday 18:45'`
stop_time=`TZ='America/Los_Angeles' date +'%s' --date='friday 00:00'`
#start_time=`TZ='America/Los_Angeles' date +'%s' --date='thursday 23:45'`
#stop_time=`TZ='America/Los_Angeles' date +'%s' --date='friday 5:00'`
#start_time=`TZ='America/Los_Angeles' date +'%s' --date='friday 8:45'`
#stop_time=`TZ='America/Los_Angeles' date +'%s' --date='friday 14:00'`



function present_time(){
	echo `TZ='America/Los_Angeles' date +'%s'`
}

while [ $start_time -gt $(( $(present_time) )) ]; do
	countdown_secs="$(( $start_time - $(present_time) ))"
	countdown=`TZ='GST+0' date --date=@$countdown_secs +"%T"`
	printf "%+10s %s %s - %s - %s\n" `TZ='America/Los_Angeles' date +'%a %R %Z'` "Waiting" "$countdown"
	sleep 300
done

while [ $stop_time -gt $(( $(present_time) )) ]; do
	until yt-dlp -q -S "res:480" https://www.twitch.tv/criticalrole; do
		printf "%+10s %s %s - %s\n" `TZ='America/Los_Angeles' date +'%a %R %Z'` "Checking"
		sleep 30
		#continue;
	done
done
