#!/bin/bash
calldir=$(pwd)
cd ~/solo/media
basedir=$(pwd)
cd $calldir
cat /dev/stdin | ${basedir}/urlof.sh | uniq | xargs yt-dlp -S "height:720" --extractor-args youtube:player_client=ios -o "%(uploader)s | %(upload_date)s | %(id)s | %(duration_string)s | %(title)s.%(ext)s"
