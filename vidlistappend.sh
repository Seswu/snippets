#!/bin/bash

# Go to working directory because otherwise we will get sort of confused about things
cd ~/solo/media

# Fetch argument
channel=$1

# Initialize parameters
from_date='19900101'  # Default value; it is unlikely that videos were uploaded to an online media platform before this date.
playlist=''
videolist=''
#basedir=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
#basedir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
basedir=$(pwd)
# Set parameters according to channel chosen
case $channel in

	cr)
	playlist=https://www.youtube.com/c/criticalrole
	videolist=critrole.lst
	;;

	heartbeast)
	playlist=https://www.youtube.com/@uheartbeast
	videolist=heartbeast.lst
	;;

	uma)
	playlist=https://www.youtube.com/channel/UCXbWsGV_cjG3gOsSnNJPVlg
	videolist=uma.lst
	;;

	hippie)
	playlist=https://www.youtube.com/channel/UCnJ3QXRI-whcqJBrW-z_htw
	videolist=gecko.lst
	;;

	seadog)
	playlist=https://www.youtube.com/channel/UClIKklzZCK7VQ3Rmbojvfyw
	videolist=seadog.lst
	;;

	nadia)
	playlist=https://www.youtube.com/@tetkanadia9045
	videolist=nadia-maras-cooking.lst
	;;

	fitgreen)
	playlist=https://www.youtube.com/@FitGreenMind
	videolist=fitgreenmind-cooking.lst
	;;

	bernabanner)
	playlist=https://www.youtube.com/channel/UCSHtaUm-FjUps090S7crO4Q
	videolist=bernadette-banner_sewing-machines.lst
	;;

	keepleft)
	playlist=https://www.youtube.com/@KeepTurningLeft
	videolist=keepturningleft.lst
	;;

	multiamory)
	playlist=https://youtu.be/uU4xa7wLG4s
	videolist=multiamory.lst
	;;

	dim20)
	playlist=https://youtube.com/channel/UCC8zWIx8aBQme-x1nX9iZ0A
	videolist=dimension20.lst
	;;

	howadhd)
	playlist=https://www.youtube.com/c/HowtoADHD
	videolist=howto_adhd.lst
	;;

	enslaved)
	playlist=https://www.youtube.com/playlist?list=PLWnMeKaZKVEXr1R58Z1XztUtNh9CDkWr2
	videolist=enslaved_by_love.lst
	;;

	nts)
	playlist=https://www.youtube.com/@nevertoosmall
	videolist=never_too_small.lst
	;;

	veri)
	playlist=https://www.youtube.com/channel/UCHnyfMqiRRG1u-2MsSQLbXA
	videolist=veritasium.lst
	;;

	nts)
	playlist=https://www.youtube.com/@nevertoosmall
	videolist=nevertoosmall.lst
	;;

	nomadic)
	playlist=https://www.youtube.com/@NomadicAmbience
	videolist=nomadic_ambience.lst
	;;

	lotrad)
	playlist=https://www.youtube.com/@TheLotRadio
	videolist=lotradio.lst
	;;

	newbliss)
	playlist=https://www.youtube.com/@NewBliss
	videolist=newbliss.lst
	;;

	aragu)
	playlist=https://www.youtube.com/@aragusea
	videolist=aragusea.lst
	;;

	tasthist)
	playlist=https://www.youtube.com/@TastingHistory
	videolist=tasting_history.lst
	;;

	townsends)
	playlist=https://www.youtube.com/@townsends
	videolist=townsends.lst
	;;

	kurz)
	playlist=https://www.youtube.com/@kurzgesagt
	videolist=kurzgesagt.lst
	;;

	list)
	echo "Currently defined channels and commands:"
	cat ${basedir}/vidlistappend.sh | grep -v 'playlist-reverse' | grep -v 'basedir' | grep -v '*' | grep ')' | tr -d ')'
	#cat $0                            | grep -v 'playlist-reverse' | grep -v 'basedir' | grep -v '*' | grep ')' | tr -d ')'
	exit
	;;

	# No channel argument means the user didn't know wtf they were doing
	# Or maybe they just forgot the niftiness of this multiple-choice script
	*)
	echo "Unknown channel."
	echo "Channel given: $channel"
	exit

esac

# Figure out how up to date we are; get last date for which a title and info was stored
if [ -e $videolist ]
then
	from_date=`cat $videolist | cut -f3 -d'|' | cut -f3 -d' ' | sort -g | tail -n1`
	#cat $videolist | cut -f10 -d' ' | sort -g | tail -n1`
fi

# Tell user that we totally know what we are doing
echo "Getting entries from $channel with the list $playlist after $from_date."
echo "Information will be stored in $videolist."

# Download all new entries, append the info to the videolist file and also list them on console as they occur.
if [ $from_date ]
#then yt-dlp --playlist-reverse --extractor-args youtube:player_client=web --print "%(uploader)s | %(playlist_title)s | %(id)s %(upload_date)s | %(duration_string)s %(title)s" --dateafter $from_date $playlist | tee -a $videolist
then yt-dlp --playlist-reverse --extractor-args youtube:player_client=ios --print "%(uploader)s | %(playlist_title)s | %(id)s %(upload_date)s | %(duration_string)s %(title)s" --dateafter $from_date $playlist | tee -a $videolist
#else yt-dlp --playlist-reverse --extractor-args youtube:player_client=web --print "%(uploader)s | %(playlist_title)s | %(id)s %(upload_date)s | %(duration_string)s %(title)s" $playlist | tee -a $videolist
else yt-dlp --playlist-reverse --extractor-args youtube:player_client=ios --print "%(uploader)s | %(playlist_title)s | %(id)s %(upload_date)s | %(duration_string)s %(title)s" $playlist | tee -a $videolist
fi
